using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckHelmetActivation : MonoBehaviour
{
    public MeshRenderer skinnedMesh;
    private Material[] skinnedMaterial;
    private PlayerController Player;
    public float timmer = 0.5f;
    public float Originaltimmer = 0.5f;
    public bool isInSpace = false;
    // Start is called before the first frame update
    void Start()
    {
        Player = FindObjectOfType<PlayerController>();
        if (skinnedMesh != null)
            skinnedMaterial = skinnedMesh.materials;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && timmer != 0)
        {
            timmer-= 0.01f;
        }
        if (isInSpace)
        {
            if (other.tag == "Player" && timmer <= 0)
            {
                Player.counter = 0;
                skinnedMaterial[0].SetFloat("_DissolveAmount", Player.counter);
                Player.isHelmetOn = true;
                //Player.helmet.SetActive(true);
                timmer = Originaltimmer;
            }
            
        }
        else if (!isInSpace)
        {
            if (other.tag == "Player" && timmer <= 0)
            {

                Player.counter = 1;
                skinnedMaterial[0].SetFloat("_DissolveAmount", Player.counter);
                Player.isHelmetOn = false;
                Player.helmet.SetActive(false);
                timmer = Originaltimmer;
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        timmer = Originaltimmer;
    }
}
