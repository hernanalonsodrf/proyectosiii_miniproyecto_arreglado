using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldImpact : MonoBehaviour
{
    public GameObject impactoVFX;

    private Material mat;

    private void OnCollisionEnter(Collision collision)
    {
        //para ser mas especifico en el impacto aqui puedes agregar algo como if (gameobject.tag == "impacto enemigp/explosion") o duplicar el prefab de inpaco y agregar dos tipos si explota o si te dan normal
        var impacto = Instantiate(impactoVFX, transform) as GameObject;
        var psr = impacto.transform.GetChild(0).GetComponent<ParticleSystemRenderer>();
        mat = psr.material;
        mat.SetVector("_SphereCenter", collision.contacts[0].point);

        Destroy(impacto, 2);
    }
}
