using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHelmet : MonoBehaviour
{
    public MeshRenderer skinnedMesh;
    [SerializeField] private AudioClip clip;
    private Material[] skinnedMaterial;
    private PlayerController player;
    public float dissolveRate = 0.0125f;
    public float dissolveRate2 = 0.0125f;
    public float refreshRate = 0.025f;
    public float refreshRate2 = 0.025f;
    float numeritos = 0;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        if (skinnedMesh != null)
            skinnedMaterial = skinnedMesh.materials;
    }
    private void FixedUpdate()
    {
        if (player.counter >= 0.2f && !player.isHelmetOn)
        {
            player.helmet.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player.voiceAudioSource.PlayOneShot(clip);
            if (player.isHelmetOn)
            {
                player.isHelmetOn = false;
                //player.helmet.SetActive(false);
                StartCoroutine(DissolveCo());


            }
            else
            {
                player.isHelmetOn = true;
                player.helmet.SetActive(true);
                StartCoroutine(DissolveCo2());
            }
        }
    }

    IEnumerator DissolveCo()
    {
        if (skinnedMaterial.Length > 0)
        {
            player.counter = 0;
            while (skinnedMaterial[0].GetFloat("_DissolveAmount") < 1)
            {
                player.counter += dissolveRate;
                for (int i = 0; i < skinnedMaterial.Length; i++)
                {
                    skinnedMaterial[i].SetFloat("_DissolveAmount", player.counter);
                }
                yield return new WaitForSeconds(refreshRate);
                
            }
        }
    }

    IEnumerator DissolveCo2()
    {
        if (skinnedMaterial.Length > 0)
        {
            while (skinnedMaterial[0].GetFloat("_DissolveAmount") >= 1 && player.counter > 0 || skinnedMaterial[0].GetFloat("_DissolveAmount") <= 1 && player.counter > 0)
            {
                player.counter -= dissolveRate2;
                for (int i = 0; i < skinnedMaterial.Length; i++)
                {
                    skinnedMaterial[i].SetFloat("_DissolveAmount", player.counter);
                }
                yield return new WaitForSeconds(refreshRate2);
            }
        }


    }
}
