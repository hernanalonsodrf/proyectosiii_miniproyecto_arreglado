using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIMainMenu : MonoBehaviour
{
    [SerializeField] private GameObject blackScreen;
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject optionsPanel;
    [SerializeField] private Dropdown resolutionDropdown;
    [SerializeField] private Image muteMusicIcon;
    [SerializeField] private AudioSource audioSourceMusic;

    private Resolution[] resolutions;
    private List<Resolution> filteredResolutions;
    
    private int currentResolutionIndex = 0;
    private float currentRefreshRate;
    private bool musicMuted;
    private Color originalIconsColor;

    // Start is called before the first frame update
    void Start()
    {
        blackScreen.SetActive(false);
        optionsPanel.SetActive(false);

        resolutions = Screen.resolutions;
        filteredResolutions = new List<Resolution>();
        resolutionDropdown.ClearOptions();
        currentRefreshRate = Screen.currentResolution.refreshRate;

        musicMuted = false;
        originalIconsColor = muteMusicIcon.GetComponent<Image>().color;

        for (int i = 0; i < resolutions.Length; i++)
        {
            if (resolutions[i].refreshRate == currentRefreshRate)
            {
                filteredResolutions.Add(resolutions[i]);
            }
        }
        List<string> options = new List<string>();
        for (int i = 0; i < filteredResolutions.Count; i++)
        {
            string resolutionOption = filteredResolutions[i].width
                + "   x   " + filteredResolutions[i].height + "  " + filteredResolutions[i].refreshRate + "Hz";
            options.Add(resolutionOption);
            if (filteredResolutions[i].width == Screen.width && filteredResolutions[i].height == Screen.height)
            {
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = filteredResolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, true);
    }
    public void MuteMenuMusic()
    {
        if (musicMuted)
        {
            muteMusicIcon.GetComponent<Image>().color = originalIconsColor;
            audioSourceMusic.volume = 0.59f;
        }
        else
        {
            Color c = originalIconsColor;
            c.r = c.r + 200;
            muteMusicIcon.GetComponent<Image>().color = c;
            audioSourceMusic.volume = 0f;
        }
        musicMuted = !musicMuted;
    }
    public void PressPlay()
    {
        blackScreen.SetActive(true);
        Invoke("DeactivateBlackScreen", 0.6f);
        SceneManager.LoadScene("InGame");
    }
    private void DeactivateBlackScreen()
    {
        blackScreen.SetActive(false);
    }
    public void ExitApllication()
    {
        Application.Quit();
    }
    public void ToggleOnOptionsPanel()
    {
        mainPanel.SetActive(false);
        optionsPanel.SetActive(true);        
    }
    public void ToggleOffOptionsPanel()
    {
        optionsPanel.SetActive(false);
        mainPanel.SetActive(true);        
    }
}
