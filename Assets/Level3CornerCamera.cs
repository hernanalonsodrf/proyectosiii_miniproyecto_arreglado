using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3CornerCamera : MonoBehaviour
{
    [SerializeField] private GameObject playerCam;
    [SerializeField] private GameObject cornerCam;
    [SerializeField] private GameObject lookAtPositionAtEndingCam;
    private bool hasToLookAtFront;
    private int counter = 0;

    // Start is called before the first frame update
    void Awake()
    {
        cornerCam.SetActive(false);
        hasToLookAtFront = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!cornerCam.activeSelf)
            {
                cornerCam.SetActive(true);
                playerCam.SetActive(false);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            cornerCam.SetActive(false);
            playerCam.SetActive(true);
            hasToLookAtFront = true;
            counter = 10;
        }
    }
    private void FixedUpdate()
    {
        if (FindObjectOfType<PlayerController>().GetComponent<PlayerController>() != null)
        {
            if (hasToLookAtFront && counter > 0)
            {
                counter--;
                playerCam.transform.LookAt(lookAtPositionAtEndingCam.transform.position);
            }
            else
            {
                hasToLookAtFront = false;
            }
        }
    }
}
