﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarProgress : MonoBehaviour
{
    PlayerController playerController;
    public Image HealthBar;
    public Text municion;
    public Material[] mat;
    public Light luzVida;
    public string numeroMunicion = "∞";
    public float actualHealth = 1;
    public float maxHealth = 1;
    public float velocidad = 1;
    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        maxHealth = playerController.maxHP;
        HealthBar.fillAmount = playerController.maxHP;
        actualHealth = playerController.maxHP;
        municion.text = numeroMunicion;

    }

    // Update is called once per frame
    void Update()
    {
        if (HealthBar.fillAmount <= 1 && HealthBar.fillAmount > 0.7)
        {
            HealthBar.material = mat[0];
            luzVida.color = Color.cyan;
        }       
        else if (HealthBar.fillAmount < 0.7 && HealthBar.fillAmount > 0.3)
        {
            HealthBar.material = mat[2];
            luzVida.color = Color.yellow;
        }
        else if (HealthBar.fillAmount < 0.3)
        {
            HealthBar.material = mat[3];
            luzVida.color = Color.red;
        }
    }

    private void FixedUpdate()
    {
        actualHealth = Mathf.Clamp(playerController.currentHP, 0, playerController.maxHP);
        HealthBar.fillAmount = Mathf.Lerp(
                HealthBar.fillAmount,
                actualHealth / maxHealth,
                velocidad
            );
    }
}
