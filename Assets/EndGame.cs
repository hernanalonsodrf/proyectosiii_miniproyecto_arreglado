using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    [SerializeField] private GameObject vCam;
    [SerializeField] private GameObject ship;
    [SerializeField] private float timeToWait;
    [SerializeField] private GameObject panel;
    [SerializeField] private AudioSource shipEngineAudioSource;
    [SerializeField] private AudioClip whooshSound;

    private bool isOpen;
    private bool open;
    private PlayerController player;
    private UIManager uim;
    // Start is called before the first frame update
    private void Awake()
    {
        ship.GetComponent<Animator>().enabled = false;
        panel.SetActive(false);
        vCam.SetActive(false);
        shipEngineAudioSource.gameObject.SetActive(false);
        uim = FindObjectOfType<UIManager>();
    }
    void Start()
    {       
        isOpen = false;
        open = false;
        player = FindObjectOfType<PlayerController>();        
    }
   
    private void OnTriggerStay(Collider other)
    {       
        if (other.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !open && other.gameObject.GetComponent<CharacterController>().isGrounded)
            {
                open = true;
                if (!isOpen) 
                {
                    panel.SetActive(false);
                    player.userInput = false;
                    uim.crossHair.SetActive(false);
                    player.gameObject.SetActive(false);
                    shipEngineAudioSource.gameObject.SetActive(true);
                    vCam.SetActive(true);
                    ship.GetComponent<Animator>().enabled=true;
                    LaunchEndText();
                    Invoke("PlayWhooshSound", timeToWait * 0.8f);
                    Invoke("ToMainMenu", timeToWait);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
              panel.SetActive(true);           
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(false);
        }
    }
    private void PlayWhooshSound()
    {
        shipEngineAudioSource.PlayOneShot(whooshSound);
    }
    private void ToMainMenu()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.UnloadSceneAsync("InGame");
        SceneManager.LoadScene("MenuPrincipal");
    }
    public void LaunchEndText()
    {
        uim.ShowVictoryPanel("Victoria");       
    }
}
