using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextOnInteract : MonoBehaviour
{
    [SerializeField] private GameObject textPanel;
    [SerializeField] private GameObject buttonToInteractUIPanel;

    [SerializeField] private Text textContainer;
    [SerializeField] private string textToShow;
    private bool alreadyPressedKey;
    // Start is called before the first frame update
    void Start()
    {
        textPanel.SetActive(false);
        buttonToInteractUIPanel.SetActive(false);
        alreadyPressedKey = false;
        textContainer.text = "";
    }   
    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            buttonToInteractUIPanel.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E) && !alreadyPressedKey)
            {
                buttonToInteractUIPanel.SetActive(false);
                textPanel.SetActive(true);
                textContainer.text = textToShow;
            }            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (textPanel.activeSelf)
            {
                textContainer.text = "";
                textPanel.SetActive(false);
            }
            buttonToInteractUIPanel.SetActive(false);
        }
    }
}
