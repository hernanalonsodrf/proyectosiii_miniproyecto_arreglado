using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveMeleeWeapon : MonoBehaviour
{
    [SerializeField] private PlayerController player;
    [SerializeField] private GameObject panelInteract;
    [SerializeField] private GameObject panelMelee;
    [SerializeField] private GameObject pointerUI;
    [SerializeField] private AudioClip clip1;
    [SerializeField] private AudioClip clip2;
    private bool alreadyOpened;
    [SerializeField] private AudioSource audioSource1;
    [SerializeField] private AudioSource audioSource2;

    // Start is called before the first frame update
    void Start()
    {
        alreadyOpened = false;
        panelMelee.SetActive(false);
        panelInteract.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !alreadyOpened)
        {
            panelInteract.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        bool open = false;
        if (other.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !open && other.gameObject.GetComponent<CharacterController>().isGrounded)
            {
                pointerUI.SetActive(false);
                audioSource1.PlayOneShot(clip1);
                audioSource2.PlayOneShot(clip2);
                panelInteract.SetActive(false);
                alreadyOpened = true;
                open = true;
                panelMelee.SetActive(true);
                player.hasMeleeUnlocked = true;
                Invoke("DeactivatePanel", 3f);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panelInteract.SetActive(false);
        }
    }
    private void DeactivatePanel()
    {
        panelMelee.SetActive(false);
    }
}
