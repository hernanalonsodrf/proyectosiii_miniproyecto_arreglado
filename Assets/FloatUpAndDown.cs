using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatUpAndDown : MonoBehaviour
{    
    [SerializeField] private GameObject downMaxPos;
    [SerializeField] private GameObject topMaxPos;

    [SerializeField] private float speed;
    [SerializeField] private float minDistanceToChangePath;

    bool goingUp;

    void Start()
    {
        goingUp = true;
    }

    void Update()
    {       
        if (goingUp)
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
        else
        {
            transform.Translate(Vector3.down * Time.deltaTime * speed);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ChangePath")
        {
            goingUp=!goingUp;
        }
    }
}
