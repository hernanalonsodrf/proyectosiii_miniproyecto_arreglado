using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateTutorial : MonoBehaviour
{
    [SerializeField] public GameObject panel;
    [SerializeField] private Text tutorialPhraseContainer;
    [SerializeField] private Text keyToPressContainer;
    [SerializeField] private string textToShow;
    [SerializeField] private string keyToShow;
    [SerializeField] private bool isMouseKey;
    [SerializeField] private KeyCode key;
    [SerializeField] private int mouseButtonName;
    private bool isInRange;    

    // Start is called before the first frame update
    void Start()
    {
        isInRange = false;
        panel.SetActive(false);
        tutorialPhraseContainer.text = "";
        keyToPressContainer.text = "";       
    }

    private void Update()
    {
        if (isInRange)
        {
            if (!isMouseKey)
            {
                if (Input.GetKeyDown(key))
                {
                    tutorialPhraseContainer.text = "";
                    keyToPressContainer.text = "";
                    panel.SetActive(false);
                    gameObject.GetComponent<Collider>().enabled = false;
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(mouseButtonName))
                {
                    tutorialPhraseContainer.text = "";
                    keyToPressContainer.text = "";
                    panel.SetActive(false);
                    gameObject.GetComponent<Collider>().enabled = false;
                }
            }
        }       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isInRange = true;
            panel.SetActive(true);
            tutorialPhraseContainer.text = textToShow;
            keyToPressContainer.text = keyToShow;
        }
    }   
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isInRange = false;
            tutorialPhraseContainer.text = "";
            keyToPressContainer.text = "";
            panel.SetActive(false);
            gameObject.GetComponent<Collider>().enabled = false;
        }
    }
}
