using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugObjectWithRaycast : MonoBehaviour
{
    //Este script va a�adido al vfx de la explosi�n del disparo
    private PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (other.gameObject.GetComponent<EnemyAI>() != null)
        {
            EnemyAI enemy = other.gameObject.GetComponent<EnemyAI>();
            if (enemy.IsAlive())
            {
                if (enemy.typeOfEnemy==1)
                {
                    int i = UnityEngine.Random.Range(0,7);
                    if (i < 2)
                    {
                        Debug.Log(i);
                        enemy.gameObject.GetComponent<Animator>().Play("Stunned");
                    }
                }
               
                player.hitMarker.SetActive(true);
                Invoke("ResetMarker", 0.3f);
                enemy.DamageEnemy((int)player.riffleDmg);                
                Invoke("ResetMarker", 0.4F);
            }
        }
        GetComponent<SphereCollider>().enabled = false;
    }
    private void ResetMarker()
    {
        player.hitMarker.SetActive(false);
    }
}
