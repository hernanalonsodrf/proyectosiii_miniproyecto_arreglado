using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPPlayer : MonoBehaviour
{
    [SerializeField] private GameObject destinationPosition;
    [SerializeField] private GameObject panel;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject blackScreen;
    [SerializeField] private AudioClip tpSound;
    private bool alreadyPressedE;
    private UIManager uim;
    public bool isEndLevelTp;
    public string endLevelText;

    // Start is called before the first frame update
    void Start()
    {
        alreadyPressedE = false;
        panel.SetActive(false);        
        blackScreen.SetActive(false);
        uim = FindObjectOfType<UIManager>();
    }
   
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(true);
            if (Input.GetKey(KeyCode.E) && !alreadyPressedE)
            {
                player.GetComponent<PlayerController>().voiceAudioSource.PlayOneShot(tpSound);
                alreadyPressedE = true;
                player.transform.position = destinationPosition.transform.position;
                player.GetComponent<PlayerController>().enabled = false;

                StartCoroutine(Teleport());
            }
            if (isEndLevelTp)
            {
                Invoke("VictoryPanel", 1f);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(false);
        }
    }
    IEnumerator Teleport()
    {
        blackScreen.SetActive(true);        
        yield return new WaitForSeconds(1f);
        Invoke("ResetScreen", 1f);
        player.GetComponent<PlayerController>().enabled = true;
    }
    private void ResetScreen()
    {
        blackScreen.SetActive(false);
    }
    private void VictoryPanel()
    {
        uim.ShowVictoryPanel(endLevelText);
        Invoke("HideVictoryPanel", 1f);
    }
    private void HideVictoryPanel()
    {
        uim.HideVictoryPanel();
    }
}
