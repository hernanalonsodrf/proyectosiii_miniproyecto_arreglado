using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAtmosphericSound : MonoBehaviour
{
    [SerializeField] private GameObject playerObject;
    [SerializeField] private AudioClip newSoundToPlay;
    [SerializeField] private AudioSource audioSource;
    private AudioClip oldClipThatWasPlaying;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            oldClipThatWasPlaying = audioSource.clip;
            audioSource.clip = newSoundToPlay;
            audioSource.Play();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            audioSource.clip = oldClipThatWasPlaying;
            audioSource.Play();
        }
    }

}
