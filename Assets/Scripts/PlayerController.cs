using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{    
    private Animator animator;
    private Vector2 input;
    private CharacterController controller;

    [SerializeField] public float movementSpeed;
    [SerializeField] private float sprintingSpeed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float rotationOffset;
    [SerializeField] private float jumpHeight;
    [SerializeField] private int minFOV;
    [SerializeField] private int maxFOV;
    [SerializeField] private int zoomInAimingSpeed;
    [SerializeField] private float zoomInSprintSpeed;
    [SerializeField] private float timeToWaitForMeleeEffect;
    [SerializeField] private CinemachineFreeLook followPlayerCAM;
    [SerializeField] private GameObject[] shootParticlesGameObjects;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    [SerializeField] private Transform vfxExplosion;
    [SerializeField] private GameObject vignetteGameObject;
    [SerializeField] public GameObject helmet;
    [SerializeField] public GameObject hitMarker;
    [SerializeField] public ParticleSystem healingParticles;
    [SerializeField] private ParticleSystem meleeParticles;
    [SerializeField] private GameObject playerMeleeWeaponCollider;

    [Header("Stats")]
    [SerializeField] public float maxHP;
    [SerializeField] public float riffleDmg;
    [SerializeField] public float pistolDmg;
    public float currentHP;

    [Header("Sounds")]
    [SerializeField] private AudioSource weaponAudioSource;
    [SerializeField] public AudioSource voiceAudioSource;
    [SerializeField] private AudioClip[] footstepClips;
    [SerializeField] private AudioClip[] rifleClips;
    [SerializeField] private AudioClip[] beingHitSounds;
    [SerializeField] private AudioClip[] dyingSounds;
    [SerializeField] private AudioClip[] jumpingSounds;
    [SerializeField] private AudioClip enterSprintSound;
    [SerializeField] private AudioClip healingSound;
    [SerializeField] private AudioClip punchingSound;
    [SerializeField] private AudioClip meleeChargeUpSound;

    [Header("Camera Options")]
    [SerializeField] private float shakeCameraIntensity;
    [SerializeField] private float shakeTimeOnStun;

    private Camera mainCamera;
    private bool isAiming;
    private bool isSprinting;
    private Vector3 dir;
    private float gravity = 9.8f;
    private float verticalSpeed = 0;
    [SerializeField] private Transform debugTransform;
    private Image vignette;
    private AudioSource audioSource;
    private bool canShoot;
    private bool isAlive;
    private bool canDie;
    public int fallingCounter;
    private bool canJump;
    private UIManager uiM;
    public bool userInput;

    //activar el casco al salir al espacio
    public bool isHelmetOn;
    public float counter = 1;
    public bool hasMeleeUnlocked;

    private void Start()
    {        
        animator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        mainCamera = Camera.main;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isAiming = false;
        hitMarker.SetActive(false);
        dir = Vector3.zero;
        foreach (GameObject o in shootParticlesGameObjects)
        {
            o.SetActive(false);
        }       
        vignette = vignetteGameObject.GetComponent<Image>();
        var tempColor = vignette.color;
        tempColor.a = 0f;
        vignette.color = tempColor;
        audioSource = GetComponent<AudioSource>();
        canShoot = true;
        isAlive = true;
        canDie = true;
        currentHP = maxHP;
        fallingCounter = 0;
        userInput = true;
        canJump = true;
        helmet.SetActive(false);
        uiM = FindObjectOfType<UIManager>();
        healingParticles.Stop();
        meleeParticles.Stop();
        playerMeleeWeaponCollider.SetActive(false);
        hasMeleeUnlocked = false;
        isHelmetOn = false;
    }

    private void Update()
    {
        if(userInput && !animator.GetCurrentAnimatorStateInfo(0).IsName("PushButton"))
        {
            if (currentHP <= 0 && canDie)
            {
                Die();
            }
            if (isAlive)
            {
                Vector3 mouseWorldPosition = Vector3.zero;
                Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
                Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
                Transform hitTransform = null;
                if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
                {
                    debugTransform.position = raycastHit.point;
                    mouseWorldPosition = raycastHit.point;
                    hitTransform = raycastHit.transform;
                }
                if (Input.GetMouseButtonDown(1) && !isSprinting)
                {
                    isAiming = true;
                }
                if (Input.GetMouseButtonUp(1))
                {
                    isAiming = false;
                }
                if (Input.GetKeyDown(KeyCode.LeftShift)
                        && !animator.GetCurrentAnimatorStateInfo(0).IsName("Stunned")
                        && !animator.GetCurrentAnimatorStateInfo(0).IsName("PushButton")
                        && !animator.GetCurrentAnimatorStateInfo(0).IsName("MeleeHit"))
                {
                    isAiming = false;
                    isSprinting = true;
                    voiceAudioSource.PlayOneShot(enterSprintSound);
                    var tempColor = vignette.color;
                    tempColor.a = 0f;
                    vignette.color = tempColor;
                }
                if (Input.GetKeyUp(KeyCode.LeftShift))
                {
                    isSprinting = false;
                }
                if (Input.GetMouseButtonDown(0) && !isSprinting)
                {
                    if (canShoot 
                        && !animator.GetCurrentAnimatorStateInfo(0).IsName("Stunned")
                        && !animator.GetCurrentAnimatorStateInfo(0).IsName("PushButton")
                        && !animator.GetCurrentAnimatorStateInfo(0).IsName("MeleeHit"))
                    {
                        Shoot(hitTransform);
                    }
                }
                if (Input.GetKeyDown(KeyCode.F) && !isSprinting && hasMeleeUnlocked //MELEE
                     && !animator.GetCurrentAnimatorStateInfo(0).IsName("Stunned")
                     && !animator.GetCurrentAnimatorStateInfo(0).IsName("PushButton")
                     && !animator.GetCurrentAnimatorStateInfo(0).IsName("MeleeHit")
                     && !animator.GetCurrentAnimatorStateInfo(0).IsName("Jump")
                     && !animator.GetCurrentAnimatorStateInfo(0).IsName("FreeFall"))
                {
                    ActivateMeleeParticles();
                    Invoke("ActivateMeleeChargeUpSound", 0.2f);
                    animator.CrossFade("MeleeHit", 0.1f);
                }

                input.x = Input.GetAxis("Horizontal");
                input.y = Input.GetAxis("Vertical");                 

                if (controller.isGrounded)//Aplicar Movimiento Vertical
                {
                    verticalSpeed = 0;
                    if (Input.GetKey(KeyCode.Space) && canJump && !isSprinting && animator.GetCurrentAnimatorStateInfo(0).IsName("Locomotion"))
                    {//SALTA
                        canJump = false;
                        verticalSpeed = Mathf.Sqrt(jumpHeight * 2 * gravity);
                        voiceAudioSource.PlayOneShot(GetRandomClip(jumpingSounds));
                        animator.SetTrigger("Jump");
                        Invoke("ResetCanJump", 0.2f);
                    }
                }
                else//CAE
                {
                    verticalSpeed -= gravity * Time.deltaTime;
                }
                Vector3 gravityMove = new Vector3(0, verticalSpeed, 0);

                dir = (transform.forward * input.y + transform.right * input.x).normalized;

                animator.SetFloat("inputX", input.x);
                animator.SetFloat("inputY", input.y);

                if (dir.magnitude >= 0.1f)//Si se mueve, mov horizontal + vertical
                {
                    if (isSprinting)
                    {
                        animator.SetBool("isSprinting", true);
                        controller.Move((dir + gravityMove) * sprintingSpeed * Time.deltaTime);
                    }
                    else
                    {
                        animator.SetBool("isSprinting", false);
                        controller.Move((dir + gravityMove) * movementSpeed * Time.deltaTime);
                    }
                }
                else//Si no se mueve, solo movimiento vertical
                {
                    controller.Move((gravityMove) * movementSpeed * Time.deltaTime);
                }
            }
            float yawCamera = mainCamera.transform.eulerAngles.y;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, yawCamera + rotationOffset, 0), rotationSpeed * Time.deltaTime);
        }       
    }
    private void FixedUpdate()
    {
        if (isAlive)
        {
            float fov = followPlayerCAM.m_Lens.FieldOfView;
            var tempColor = vignette.color;

            if (fallingCounter > 60 && !animator.GetCurrentAnimatorStateInfo(0).IsName("PushButton"))
            {
                animator.Play("FreeFall");
            }          

            if ((isAiming || isSprinting) && fov >= minFOV)
            {
                if (isAiming)
                {
                    followPlayerCAM.m_Lens.FieldOfView -= zoomInAimingSpeed;
                }
                else
                {
                    followPlayerCAM.m_Lens.FieldOfView -= zoomInSprintSpeed;
                }
            }
            if (!isAiming && !isSprinting && fov < maxFOV)
            {
                followPlayerCAM.m_Lens.FieldOfView += zoomInAimingSpeed;
            }
            if (isSprinting && tempColor.a < .7)
            {
                tempColor.a = tempColor.a + 0.1f;
                vignette.color = tempColor;
            }
            if (!isSprinting && tempColor.a > 0f)
            {
                tempColor.a = 0f;
                vignette.color = tempColor;
            }
            if (!controller.isGrounded)
            {
                fallingCounter++;
            }
            else
            {
                fallingCounter = 0;
            }
        }
        
    }

    private void Die()
    {
        if (currentHP < 0)
        {
            currentHP = 0;
        }
        isAlive = false;
        canDie = false;
        voiceAudioSource.PlayOneShot(GetRandomClip(dyingSounds));
        animator.Play("Death");
        Invoke("CallDeathUI", 1.5f);
    }  
    private void CallDeathUI()
    {
        uiM.DeathUI();
    }    
    private void Shoot(Transform hitTransform)
    {
        canShoot = false;
        weaponAudioSource.PlayOneShot(GetRandomClip(rifleClips));
        Invoke("ActivateShootParticles", 0.4f);
        StartCoroutine(ExplosionCoroutine(hitTransform));        
    }
    private void ResetFOVafterShooting()
    {
        followPlayerCAM.m_Lens.FieldOfView = followPlayerCAM.m_Lens.FieldOfView + 0.5f;
    }
    private void ResetFOVafterShootingWhileMoving()
    {
        followPlayerCAM.m_Lens.FieldOfView = followPlayerCAM.m_Lens.FieldOfView + 1.2f;
    }
    public void DamagePlayer(int dmg)
    {
        currentHP -= dmg;
    }
    IEnumerator ExplosionCoroutine(Transform hitTransform)
    {
        yield return new WaitForSeconds(1);

        if (hitTransform != null)
        {                   
            Transform o = Instantiate(vfxExplosion, debugTransform.position, Quaternion.identity);
            StartCoroutine(DeleteUnusedExplosions(o));
        }
    }
    IEnumerator DeleteUnusedExplosions(Transform o)
    {
        yield return new WaitForSeconds(5);
        GameObject.Destroy(o.gameObject);
    }
    private void ActivateShootParticles()
    {       
        foreach (GameObject o in shootParticlesGameObjects)
        {
            o.SetActive(true);
        }
        Invoke("ResetParticles", 0.5f);
    }
    private void ResetParticles()
    {
        if ((Mathf.Abs(animator.GetFloat("inputX")) > 0)||(Mathf.Abs(animator.GetFloat("inputY"))> 0))
        {
            followPlayerCAM.m_Lens.FieldOfView = followPlayerCAM.m_Lens.FieldOfView - 1.2f;           
            Invoke("ResetFOVafterShootingWhileMoving", 0.06f);
        }
        else
        {
            followPlayerCAM.m_Lens.FieldOfView = followPlayerCAM.m_Lens.FieldOfView - 0.5f;
            Invoke("ResetFOVafterShooting", 0.06f);            
        }       
        foreach (GameObject o in shootParticlesGameObjects)
        {
            o.SetActive(false);
        }
        canShoot = true;
    }
    private void ActivateMeleeParticles()
    {
        meleeParticles.Play();
        Invoke("DeactivateMeleeParticles", timeToWaitForMeleeEffect);
    }
    private void DeactivateMeleeParticles()
    {
        meleeParticles.Stop();
    }
    private void ResetCanJump()
    {
        canJump = true;
    }
    private void Step()
    {
        audioSource.PlayOneShot(GetRandomClip(footstepClips));
    }
    private void SwingingPunchSound()
    {
        audioSource.PlayOneShot(punchingSound);
    }
    private AudioClip GetRandomClip(AudioClip[] clips)
    {
         return clips[UnityEngine.Random.Range(0, clips.Length)];        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnemyWeapon" && IsAlive() 
            && !animator.GetCurrentAnimatorStateInfo(0).IsName("MeleeHit"))
        {
            DeactivateMeleeWeapon();
            animator.Play("Stunned");
            voiceAudioSource.PlayOneShot(GetRandomClip(beingHitSounds));
            DamagePlayer(other.GetComponentInParent<EnemyAI>().GetDamage());            
        }
        if (other.tag == "DeathCollider")
        {
            DeactivateMeleeWeapon();
            DieFloating();
        }
        if (other.tag == "Potion" && IsAlive())
        {
            if(currentHP + maxHP * 0.3f < maxHP)
            {
                currentHP = currentHP + maxHP * 0.3f;
            }
            else
            {
                currentHP = maxHP;
            }
            
            voiceAudioSource.PlayOneShot(healingSound);
            Destroy(other.gameObject);
            healingParticles.Play();
            Invoke("ResetHealingParticles", 1.5f);
        }
    }
    private void ActivateMeleeWeapon()
    {
        playerMeleeWeaponCollider.SetActive(true);
    }
    private void ActivateMeleeChargeUpSound()
    {
        weaponAudioSource.PlayOneShot(meleeChargeUpSound);
    }
    private void DeactivateMeleeWeapon()
    {
        playerMeleeWeaponCollider.SetActive(false);
    }
    private void ResetHealingParticles()
    {
        healingParticles.Stop();
    }
    public bool IsAlive()
    {
        return this.isAlive;
    }
    private void DieFloating()
    {
        if (currentHP < 0)
        {
            currentHP = 0;
        }
        isAlive = false;
        canDie = false;
        voiceAudioSource.PlayOneShot(enterSprintSound);
        animator.Play("FloatingDeath");
        Invoke("CallDeathUI", 1.5f);
        //REVIVIR
    }    
}

