using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgitarCamara : StateMachineBehaviour
{
    private CameraShakeCinemachine cameraShake;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetBool("IsLockedOn"))
        {
            cameraShake = GameObject.Find("PlayerLockOnCamera").GetComponent<CameraShakeCinemachine>();
            cameraShake.ShakeCamera(1f, 0.1f);
        }
        else
        {
            cameraShake = GameObject.Find("PlayerFollowCamera").GetComponent<CameraShakeCinemachine>();
            cameraShake.ShakeCamera(1f, 0.1f);
        }       
    }

}
