using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeactivateOnTrigger: MonoBehaviour
{
    [SerializeField] public GameObject[] thingsToDeactivate;    

    void OnTriggerEnter(Collider other)
    {
	if(other.tag == "Player")
	{
		foreach(GameObject o in thingsToDeactivate)
		{
			o.SetActive(false);
		}
	}        

    }
    void OnTriggerExit(Collider other)
    {
	if(other.tag == "Player")
	{
		gameObject.GetComponent<Collider>().enabled = false;
	}	
    }
}
