using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField] public GameObject crossHair;
    [SerializeField] private GameObject escapePanel;
    [SerializeField] private GameObject cheatsPanel;
    [SerializeField] private GameObject blackScreen;
    [SerializeField] private GameObject deathPanel;
    [SerializeField] private GameObject optionsInGamePanel;
    [SerializeField] private GameObject muteStepsIcon;
    [SerializeField] private GameObject muteShotsIcon;
    [SerializeField] private GameObject muteEnemiesIcon;
    [SerializeField] private GameObject player;  
    [SerializeField] private GameObject victoryPanel;
    [SerializeField] private Text victoryText;
    [SerializeField] public GameObject[] enemies;
    [SerializeField] private AudioSource weaponAudioSource;
    [SerializeField] private Dropdown resolutionDropdown;

    private List<AudioSource> enemyVoiceSounds;
    private List<AudioSource> enemyFootStepSounds;
    private AudioSource playerSteps;
    private Resolution[] resolutions;
    private List<Resolution> filteredResolutions;

    private bool inPause;
    private bool stepsMuted;
    private bool shotsMuted;
    private bool enemiessMuted;
    private Color originalIconsColor;
    private int currentResolutionIndex = 0;
    private float currentRefreshRate;

    void Start()
    {
        victoryPanel.SetActive(false);
        deathPanel.SetActive(false);
        optionsInGamePanel.SetActive(false);
        blackScreen.SetActive(false);
        escapePanel.SetActive(false);
        inPause = false;
        enemiessMuted = false;
        shotsMuted = false;
        stepsMuted = false;
        originalIconsColor = muteStepsIcon.GetComponent<Image>().color;
        enemyVoiceSounds = new List<AudioSource>();
        enemyFootStepSounds = new List<AudioSource>();
        playerSteps = player.GetComponent<AudioSource>();
        
        resolutions = Screen.resolutions;
        filteredResolutions = new List<Resolution>();
        resolutionDropdown.ClearOptions();
        currentRefreshRate = Screen.currentResolution.refreshRate;

        for (int i = 0; i < resolutions.Length; i++)
        {
            if(resolutions[i].refreshRate == currentRefreshRate)
            {
                filteredResolutions.Add(resolutions[i]);
            }
        }
        List<string> options = new List<string>();
        for(int i = 0; i < filteredResolutions.Count; i++)
        {
            string resolutionOption = filteredResolutions[i].width 
                + "   x   " + filteredResolutions[i].height + "  " + filteredResolutions[i].refreshRate + "Hz";
            options.Add(resolutionOption);
            if (filteredResolutions[i].width == Screen.width && filteredResolutions[i].height == Screen.height)
            {
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        foreach (GameObject o in enemies)
        {            
            enemyVoiceSounds.Add(o.GetComponent<EnemyAI>().voiceAudioSource);
        }
        foreach (GameObject o in enemies)
        {
            enemyFootStepSounds.Add(o.GetComponent<AudioSource>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (inPause)
            {
                Resume();
            }
            else
            {
                if (!deathPanel.activeSelf && !cheatsPanel.activeSelf)
                {
                    Pause();
                }               
            }            
        }       
    }
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = filteredResolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, true);
    }
    public void Resume()
    {        
        inPause = false;
        optionsInGamePanel.SetActive(false);
        escapePanel.SetActive(false);       
        Time.timeScale = 1f;
        crossHair.SetActive(true);        
        player.GetComponent<PlayerController>().userInput = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (shotsMuted)
        {
            weaponAudioSource.volume = 0f;
        }
        else
        {
            weaponAudioSource.volume = 1f;
        }
    }
    private void Pause()
    {
        weaponAudioSource.volume = 0f;
        inPause = true;
        escapePanel.SetActive(true);
        crossHair.SetActive(false);
        Time.timeScale = 0.01f;
        player.GetComponent<PlayerController>().userInput = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void Restart()
    {
        deathPanel.SetActive(false);
        blackScreen.SetActive(true);
        Invoke("DeactivateBlackScreen", 0.6f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ShowVictoryPanel(string str)
    {
        victoryPanel.SetActive(true);
        victoryText.text = str;
    }
    public void HideVictoryPanel()
    {
        victoryText.text = "";
        victoryPanel.SetActive(false);
    }
    public void DeathUI()
    {
        crossHair.SetActive(false);
        deathPanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void ExitGame()
    {
        inPause = false;
        escapePanel.SetActive(false);
        Time.timeScale = 1f;
        crossHair.SetActive(true);
        player.GetComponent<PlayerController>().userInput = true;
        SceneManager.UnloadSceneAsync("InGame");
        SceneManager.LoadScene("MenuPrincipal");
    }
    public void ExitApllication()
    {
        Application.Quit();
    }
    private void DeactivateBlackScreen()
    {
        blackScreen.SetActive(false);
    }
    public void MuteFootsteps()
    {
        if (stepsMuted)
        {
            muteStepsIcon.GetComponent<Image>().color = originalIconsColor;
            foreach (AudioSource a in enemyFootStepSounds)
            {
                if(a!= null)
                {
                    a.volume = 1f;
                }              
            }
            playerSteps.volume = 0.281f;
        }
        else
        {
            Color c = originalIconsColor;
            c.r = c.r + 200;
            muteStepsIcon.GetComponent<Image>().color = c;
            foreach (AudioSource a in enemyFootStepSounds)
            {
                if (a != null)
                {
                    a.volume = 0f;
                }
            }
            playerSteps.volume = 0f;
        }
        stepsMuted = !stepsMuted;
    }
    
    public void MuteEnemyVoices()
    {
        if (enemiessMuted)
        {   
            muteEnemiesIcon.GetComponent<Image>().color = originalIconsColor;
            foreach (AudioSource a in enemyVoiceSounds)
            {
                if (a != null)
                {
                    a.volume = 0.576f;
                }
            }
        }
        else
        {
            Color c = originalIconsColor;
            c.r = c.r + 200;
            muteEnemiesIcon.GetComponent<Image>().color = c;
            foreach (AudioSource a in enemyVoiceSounds)
            {
                if (a != null)
                {
                    a.volume = 0f;
                }
            }
        }
        enemiessMuted = !enemiessMuted;
    }   
    public void MuteShots()
    {
        if (shotsMuted)
        {
            muteShotsIcon.GetComponent<Image>().color = originalIconsColor;
            weaponAudioSource.volume = 1f;
        }
        else
        {
            Color c = originalIconsColor;
            c.r = c.r + 200;
            muteShotsIcon.GetComponent<Image>().color = c;
            weaponAudioSource.volume = 0f;
        }
        shotsMuted = !shotsMuted;
    }    
    public void OpenInGameOptionsPanel()
    {
        optionsInGamePanel.SetActive(true);
    }
    public void CloseInGameOptionsPanel()
    {
        optionsInGamePanel.SetActive(false);
    }
    public bool IsinPause()
    {
        return this.inPause;
    }
    public bool IsInDead()
    {
        if (deathPanel.activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
