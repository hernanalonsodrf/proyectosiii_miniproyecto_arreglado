using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    [Header("AI Behaviour")]
    [SerializeField] private float meleeRange;
    [SerializeField] private float sightRange;
    [SerializeField] private float explodingRange;
    [SerializeField] private float damping;

    [Header("0-Explodes, 1-Melee, 3-Ranged")]
    [SerializeField] public int typeOfEnemy;

    [Header("Stats")]
    [SerializeField] private int maxHP;
    [SerializeField] private int damage;
    [SerializeField] private GameObject weaponcollider;

    [Header("VFX")]
    [SerializeField] private GameObject explosionVFX;

    [Header("Sounds")]
    [SerializeField] private AudioClip[] footstepClips;
    [SerializeField] private AudioClip[] gettingHitSounds;
    [SerializeField] private AudioClip[] dyingtSounds;
    [SerializeField] private AudioClip[] speechSounds;
    [SerializeField] private AudioClip[] laughSounds;
    [SerializeField] private AudioClip[] meleeAttackSounds;
    [SerializeField] private AudioClip[] expodingSounds;
    [SerializeField] public AudioSource voiceAudioSource;
    [SerializeField] private AudioClip punchHitSound;

    private float currentHP;
    private NavMeshAgent agent;
    private GameObject player;
    private Animator animator;
    private bool isAlive;
    private bool canDie;
    private AudioSource audioSource;
    private Rigidbody rg;
    private bool canTalk;
    private bool canLaugh;
    private bool canAttackSound;
    private UIManager uim;
    public bool canMove;

    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
        isAlive = true;
        agent = GetComponent<NavMeshAgent>();
        player = FindObjectOfType<PlayerController>().gameObject;
        animator = GetComponent<Animator>();
        canDie = true;
        if(explosionVFX != null) explosionVFX.SetActive(false);
        audioSource=GetComponent<AudioSource>();
        rg = GetComponent<Rigidbody>();        
        if (weaponcollider != null) weaponcollider.SetActive(true);
        canTalk = true;
        canLaugh = true;
        animator.SetBool("isMoving", false);
        canMove = true;
        uim = FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        rg.angularVelocity = Vector3.zero;
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if (currentHP <= 0 && canDie)
        {
            Die();
        }
        
        if (isAlive && canMove)
        {           
            if (distance <= sightRange && player.GetComponent<PlayerController>().IsAlive())
            {
                var lookPos = player.transform.position - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
                if (distance <= meleeRange)
                {
                    if(typeOfEnemy == 0)
                    {
                        Explode();
                    }else if(typeOfEnemy == 1)
                    {
                        AttackPlayer();
                    }else if (typeOfEnemy == 3)
                    {

                    }
                }
                else
                {
                    if (typeOfEnemy != 3) FollowPlayer();                   
                }                
            }
            else
            {
                if(!player.GetComponent<PlayerController>().IsAlive() && typeOfEnemy == 1 && canLaugh)
                {
                    canLaugh = false;
                    voiceAudioSource.PlayOneShot(GetRandomClip(laughSounds));
                }
                animator.SetBool("isMoving", false);
            }
        }
        else
        {
            agent.isStopped = true;
        }
    }
    private void FixedUpdate()
    {
        if(canTalk && isAlive) StartCoroutine(Speech());        
    }
    private void FollowPlayer()
    {        
            animator.SetBool("isMoving", true);
            agent.isStopped = false;
            agent.SetDestination(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));            
    }
    private void AttackPlayer()
    {       
        agent.isStopped = true;        
        animator.SetTrigger("Attack");        
    }
    private void Explode()
    {
        GetComponent<CapsuleCollider>().enabled = false;
        audioSource.PlayOneShot(GetRandomClip(expodingSounds));
        explosionVFX.SetActive(true);
        animator.SetTrigger("Explode");
        if (Vector3.Distance(transform.position, player.transform.position) <= explodingRange)
        {
            player.GetComponent<PlayerController>().DamagePlayer(damage);
        }
        Die();
    }
    private void Die()
    {
        audioSource.PlayOneShot(GetRandomClip(dyingtSounds));
        if(weaponcollider != null) DeactivateWeaponCollider();
        if (GetComponent<CapsuleCollider>() != null)
        {
            CapsuleCollider col = GetComponent<CapsuleCollider>();
            col.height = col.height / 2;
            col.center = new Vector3(col.center.x, col.center.y / 2.5f, col.center.z - 50f);
        }
        if (GetComponent<BoxCollider>() != null)
        {
            BoxCollider col = GetComponent<BoxCollider>();
            col.enabled = false;
        }
        if (currentHP < 0)
        {
            currentHP = 0;
        }
        isAlive = false;
        canDie = false;
        animator.Play("Death");
        if (typeOfEnemy == 0)
        {
            Invoke("DestroyEnemy", 3f);
        }        
    }
    private void DestroyEnemy()
    {
       for (int i = 0; i < uim.enemies.Length; i++)
        {
            if (uim.enemies[i] == gameObject)
            {
                uim.enemies[i] = null;
            }
        }
        GameObject.Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerWeapon")//Solo gestiona golpes a melee
        {     //los disparos del jugador se gestionan en DebugObjectWithRaycast      
            if (IsAlive())
            {
                if (typeOfEnemy == 1)
                {   
                    audioSource.PlayOneShot(punchHitSound);
                    animator.Play("Stunned");                    
                }
                player.GetComponent<PlayerController>().hitMarker.SetActive(true);
                Invoke("ResetMarker", 0.3f);
                DamageEnemy((int)player.GetComponent<PlayerController>().riffleDmg);
                Invoke("ResetMarker", 0.4F);
            }
        }
    }
    public void DamageEnemy(int dmg)
    {
        audioSource.PlayOneShot(GetRandomClip(gettingHitSounds));
        currentHP -= dmg;
    }
    IEnumerator Speech()
    {
        canTalk = false;
        yield return new WaitForSeconds(2);
        int i = UnityEngine.Random.Range(0, 2);        
        if (i == 1)
        {
            if(!voiceAudioSource.isPlaying) voiceAudioSource.PlayOneShot(GetRandomClip(speechSounds));
        }
        StartCoroutine(ResetCanTalk());

    }
    IEnumerator ResetCanTalk()
    {
        yield return new WaitForSeconds(1);
        canTalk = true;
    }   
    private void Step()
    {
        audioSource.PlayOneShot(GetRandomClip(footstepClips));
    }
    private AudioClip GetRandomClip(AudioClip[] clips)
    {
        return clips[UnityEngine.Random.Range(0, clips.Length)];
    }
    public void ActivateWeaponCollider()
    {
        weaponcollider.SetActive(true);
    }
    public void DeactivateWeaponCollider()
    {
        weaponcollider.SetActive(false);
    }
    public int GetDamage()
    {
        return this.damage;
    }
    public bool IsAlive()
    {
        return this.isAlive;
    }
    public void PlayMeleeAttackSound()
    {
        audioSource.PlayOneShot(GetRandomClip(meleeAttackSounds));
    }
    private void ResetMarker()
    {
        player.GetComponent<PlayerController>().hitMarker.SetActive(false);
    }
}
