using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateEnemiesOnFireAGroup : MonoBehaviour
{
    public EnemyAI[] enemies;
    private int counter = 0;
    private bool switch1=true;
    private bool switch2 = true;   

    // Update is called once per frame
    void Update()
    {
        if (switch1)
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i].isBeingShotFromFarAway)
                {
                    counter++;
                    switch1 = false;
                }
            }
        }
        
        if (switch2 && counter > 0)
        {
            foreach (EnemyAI e in enemies)
            {
                e.isBeingShotFromFarAway = true;
            }
            switch2 = false;
        }        
    }
}
