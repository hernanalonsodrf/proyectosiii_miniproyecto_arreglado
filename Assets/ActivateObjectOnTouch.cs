using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjectOnTouch : MonoBehaviour
{
    [SerializeField] private GameObject[] objectsToActivate;
    
    void Start()
    {
        foreach (GameObject o in objectsToActivate)
        {
            o.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            foreach(GameObject o in objectsToActivate)
            {
                o.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject, 1);
        }
    }
}
