using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] private Transform topPart;
    [SerializeField] public GameObject panel;
    [SerializeField] private float speed;
    [SerializeField] private float waitingTimeToOpen;
    [SerializeField] private float waitingTimeToSound;
    [SerializeField] private AudioClip openConfirmationSound;
    [SerializeField] private AudioClip openingSound;
    [SerializeField] private AudioSource openingTerminalAudioSource;
    [SerializeField] private GameObject[] objectsToActivate;
    [SerializeField] private GameObject[] objectsToDeactivate;
    [SerializeField] private bool isHangar;
    [SerializeField] private GameObject panelTutorial;

    private bool isOpen;
    private bool isAlreadyUp;
    private AudioSource audioSource;
    private bool isLookingAtTerminal;
    private int counter;
    private bool alreadyActivated;
    private int shipCounter;    
    
    void Start()
    {       
        isOpen = false;
        isAlreadyUp = false;
        alreadyActivated = false;
        panel.SetActive(false);
        shipCounter = 0;
        audioSource = GetComponent<AudioSource>();
        isLookingAtTerminal = false;
        counter = 90;
        if (objectsToActivate != null)
        {
            if (isHangar)
            {
                foreach (GameObject o in objectsToActivate)
                {
                    o.GetComponent<SphereCollider>().enabled=false;
                }
            }
            else
            {
                foreach (GameObject o in objectsToActivate)
                {
                    o.SetActive(false);
                }
            }            
        }   
        
    }    
    void Update()
    {        
        if (Vector3.Distance(transform.position, topPart.position) < 0.01f)
            {
                isOpen = false;
                isAlreadyUp = true;
            }
         if (isOpen)
         {
            transform.position = Vector3.MoveTowards(transform.position, topPart.position, speed * Time.deltaTime);
         }             
    }
    private void FixedUpdate()
    {
        if (FindObjectOfType<PlayerController>().GetComponent<PlayerController>() != null)
        {
            PlayerController player = FindObjectOfType<PlayerController>().GetComponent<PlayerController>();
            if (isLookingAtTerminal && counter > 0)
            {
                if (player != null)
                {
                    player.userInput = false;
                    player.fallingCounter = 0;

                    counter--;
                    player.gameObject.transform.LookAt(openingTerminalAudioSource.transform.position);
                }
            }
            else
            {
                if (player != null)
                {
                    player.userInput = true;
                }
            }
        }       
    }
    private void OnTriggerStay(Collider other)
    {
        bool open = false;
        if(other.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !open && other.gameObject.GetComponent<CharacterController>().isGrounded)
            {              
                open = true;
                if (!isOpen && !isAlreadyUp)
                {
                    if (isHangar)
                    {
                        foreach (GameObject o in objectsToActivate)
                        {
                            o.GetComponent<SphereCollider>().enabled = true;
                        }
                    }
                    else
                    {
                        foreach (GameObject o in objectsToActivate)
                        {
                            o.SetActive(true);
                        }
                    }
                    if (objectsToDeactivate != null)
                    {
                        if (objectsToActivate != null)
                        {
                            foreach (GameObject o in objectsToDeactivate)
                            {
                                o.SetActive(false);
                            }
                        }
                    }                   
                    isLookingAtTerminal = true;
                    other.gameObject.GetComponent<Animator>().Play("PushButton");
                    alreadyActivated = true;                   
                    Invoke("WaitToOpen", waitingTimeToOpen);
                    Invoke("ActivateSounds", waitingTimeToSound);
                    Invoke("DestroyDoor", 15f);
                }
            }
        }        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !alreadyActivated)
        {
            if (!isAlreadyUp)
            {
                if (panelTutorial != null)
                {
                    if (panelTutorial.activeSelf)
                    {
                        panelTutorial.SetActive(false);
                    }
                }
                panel.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(false);
        }           
    }   
    private void ActivateSounds()
    {
        openingTerminalAudioSource.PlayOneShot(openConfirmationSound);
        audioSource.PlayOneShot(openingSound);
    }
    private void WaitToOpen()
    {
        isOpen = true;
    }
    private void DestroyDoor()
    {
        isOpen=false;
    }
}
