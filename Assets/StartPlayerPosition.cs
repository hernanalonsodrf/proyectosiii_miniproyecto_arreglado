using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlayerPosition : MonoBehaviour
{
    [SerializeField] private Transform nv1Pos;
    [SerializeField] private Transform nv2Pos;
    [SerializeField] private Transform nv3Pos;
    [SerializeField] private GameObject cheatsPanel;
    [SerializeField] private GameObject crossHair;
    [SerializeField] private GameObject blackScreen;

    [Header("PLAYER STARTING LEVEL CHEAT")]
    [Header("The Player will start the game at this position. Mark ONLY ONE")]
    public bool nv1;
    public bool nv2;
    public bool nv3;
    [Header("Mark this to be able to move in between levels with T")]
    public bool cheats;    

    private bool inCheats;
    private UIManager uim;

    // Start is called before the first frame update
    void Start()
    {
        uim = FindObjectOfType<UIManager>();
        if (nv1)
        {
            gameObject.transform.position = nv1Pos.position;
        }
        if (nv2)
        {
            gameObject.transform.position = nv2Pos.position;
        }
        if (nv3)
        {
            gameObject.transform.position = nv3Pos.position;
        }
        inCheats = false;
        cheatsPanel.SetActive(false);
    }
    void Update()
    {
        if (cheats)
        {
            if (Input.GetKeyDown(KeyCode.T) && !uim.IsInDead() && !uim.IsinPause())
            {
                if (!inCheats)
                {
                    GetComponent<PlayerController>().userInput = false;
                    inCheats = true;
                    cheatsPanel.SetActive(true);
                    crossHair.SetActive(false);
                    Time.timeScale = 0.01f;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                }
                else
                {
                    GetComponent<PlayerController>().userInput = true;
                    inCheats = false;
                    cheatsPanel.SetActive(false);
                    crossHair.SetActive(true);
                    Time.timeScale = 1f;
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                }
            }           
        }
    }
    public void GoToLv1()
    {        
        transform.position = nv1Pos.position;
        CloseCheatUI();
        GetComponent<PlayerController>().enabled = false;
        StartCoroutine(Teleport());
    }
    public void GoToLv2()
    {
        transform.position = nv2Pos.position;
        CloseCheatUI();
        GetComponent<PlayerController>().enabled = false;
        StartCoroutine(Teleport());
    }
    public void GoToLv3()
    {
        transform.position = nv3Pos.position;
        CloseCheatUI();
        GetComponent<PlayerController>().enabled = false;
        StartCoroutine(Teleport());
    }
    public void CloseCheatUI()
    {
        GetComponent<PlayerController>().userInput = true;
        inCheats = false;
        cheatsPanel.SetActive(false);
        crossHair.SetActive(true);
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    IEnumerator Teleport()
    {
        blackScreen.SetActive(true);
        yield return new WaitForSeconds(1f);
        Invoke("ResetScreen", 1f);
        GetComponent<PlayerController>().enabled = true;
    }
    private void ResetScreen()
    {
        blackScreen.SetActive(false);
    }

}
